package com.kaya.bulletinmaker;

import com.kaya.bulletinmaker.config.DataSourceConfig;
import com.kaya.bulletinmaker.config.WebConfiguration;
import com.kaya.bulletinmaker.config.WebSecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;


@SpringBootApplication
@Import({WebSecurityConfig.class,DataSourceConfig.class, WebConfiguration.class})
public class BulletinmakerApplication {


    public static void main(String[] args) {
        SpringApplication.run(BulletinmakerApplication.class, args);
    }


}
