package com.kaya.bulletinmaker.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.*;
import java.util.Collection;
import java.util.List;


@Entity
@Data
@Table(name = "APPUSER")
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    private Long id;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PASSWORD")
    private String password;

    @OneToMany(mappedBy = "user")
    Collection<Report> userReports;

    @ManyToMany
    @JoinTable(
            name = "SUBSCRIBED_REPORTS",
            joinColumns = @JoinColumn(name ="CLIENT_USER_ID"),
            inverseJoinColumns = @JoinColumn(name="REPORT_ID")
    )
    Collection<Report> subscribedReports;

    @OneToMany(mappedBy = "user")
    Collection<TimestampInfo> modifications;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns
            = @JoinColumn(name = "user_id",
            referencedColumnName = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id",
                    referencedColumnName = "id"))
    private List<Role> roles;
}
