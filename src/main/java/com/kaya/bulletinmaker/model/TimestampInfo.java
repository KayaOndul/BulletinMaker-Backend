package com.kaya.bulletinmaker.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Data
public class TimestampInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Timestamp modification_timestamp;

    @ManyToOne
    private User user;

    @ManyToOne
    private Report report;

}
