package com.kaya.bulletinmaker.model;


import com.kaya.bulletinmaker.helpers.HashMapConverter;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Map;

@Entity
@Data
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="REPORT_ID")
    private Long id;

    @Column(name="REPORT_DATA")
    @Convert(converter = HashMapConverter.class)
    private Map<String,Object> reportData;

    @Column(name="DATE_CREATED")
    private Timestamp dateCreated;

    @Column(name="IS_ARCHIVED")
    private Boolean isArchived;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "report")
    private Collection<TimestampInfo> modificationLog;

    @ManyToMany
    private Collection<User> subscribers;


}
