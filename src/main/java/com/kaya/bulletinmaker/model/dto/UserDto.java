package com.kaya.bulletinmaker.model.dto;


import lombok.*;

@Data
public class UserDto {

    public UserDto(String email, String token) {
        this.email = email;
        this.token = token;
    }

    private String password;
    private String email;
    private String token;



}
