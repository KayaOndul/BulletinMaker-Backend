package com.kaya.bulletinmaker.controller;

import com.kaya.bulletinmaker.model.dto.UserDto;
import com.kaya.bulletinmaker.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/token")
@RestController
@AllArgsConstructor
public class AuthController {

    private UserService userService;




    @GetMapping
    public ResponseEntity<String> hello() throws Exception {
        return new ResponseEntity<>("hello", HttpStatus.ACCEPTED);
    }



    @PostMapping
    public ResponseEntity<UserDto> logIn(@RequestBody UserDto userDto) throws Exception {
//        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userDto.getEmail(), userDto.getPassword()));
        return new ResponseEntity<>(userService.login(userDto), HttpStatus.ACCEPTED);
    }


    @PostMapping("/register")
    public ResponseEntity<UserDto> register(@RequestBody UserDto userDto)   {

        return new ResponseEntity<>(userService.register(userDto), HttpStatus.CREATED);
    }

}
