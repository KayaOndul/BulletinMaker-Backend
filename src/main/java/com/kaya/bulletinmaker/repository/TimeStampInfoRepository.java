package com.kaya.bulletinmaker.repository;

import com.kaya.bulletinmaker.model.TimestampInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimeStampInfoRepository extends JpaRepository<TimestampInfo,Long> {
}
