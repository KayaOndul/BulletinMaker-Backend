package com.kaya.bulletinmaker.repository;

import com.kaya.bulletinmaker.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientUserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
}
