package com.kaya.bulletinmaker.repository;

import com.kaya.bulletinmaker.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {
}
