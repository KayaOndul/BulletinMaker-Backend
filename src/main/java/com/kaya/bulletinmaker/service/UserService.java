package com.kaya.bulletinmaker.service;

import com.kaya.bulletinmaker.jwt.JwtTokenUtil;
import com.kaya.bulletinmaker.model.User;
import com.kaya.bulletinmaker.model.dto.UserDto;
import com.kaya.bulletinmaker.repository.ClientUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;



@Service
@AllArgsConstructor
public class UserService implements IAuthService {
    private ClientUserRepository userRepository;
    private final BCryptPasswordEncoder bcryptEncoder;
    private AuthenticationManager authenticationManager;
    private JwtTokenUtil jwtTokenUtil;


    @Override
    public UserDto login(UserDto userDto) throws Exception {
         User user = userRepository.findByEmail(userDto.getEmail());
        if(user==null){
            throw new Exception("User Doesn't Exists!!");
        }

        return generateTokenResponse(user, user.getEmail(), userDto.getPassword());

    }

    @Override
    public UserDto register(UserDto userDto) {
        User newUser = new User();
        newUser.setEmail(userDto.getEmail());
        newUser.setPassword(bcryptEncoder.encode(userDto.getPassword()));
        newUser= userRepository.save(newUser);


        return generateTokenResponse(newUser, userDto.getEmail(), userDto.getPassword());
    }

    private UserDto generateTokenResponse(User newUser, String email, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        final String token = jwtTokenUtil.generateToken(newUser);
        return new UserDto(newUser.getEmail(), token);
    }


}


