package com.kaya.bulletinmaker.service;

import com.kaya.bulletinmaker.model.dto.UserDto;

public interface IAuthService {
    UserDto login (UserDto userDto) throws Exception;
    UserDto register (UserDto userDto);
}
