package com.kaya.bulletinmaker.service;

import com.kaya.bulletinmaker.model.User;
import com.kaya.bulletinmaker.repository.ClientUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;


@Service(value = "authService")
@AllArgsConstructor
public class AuthService implements UserDetailsService{
    private ClientUserRepository userRepository;



    public UserDetails loadUserByUsername(String email)  {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), getAuthority());
    }
    private List<SimpleGrantedAuthority> getAuthority() {
        return Arrays.asList(new SimpleGrantedAuthority("STANDARD_USER"));
    }

}

