package com.kaya.bulletinmaker.model;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ReportTest {

    @Test
    void serializationCheck() {
        Report report = new Report();
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("address", "123 Main Street");
        attributes.put("zipcode", 12345);

        report.setReportData(attributes);

        Map<String, Object> ed = report.getReportData();

        assertTrue(ed.containsKey("address"));

    }
}